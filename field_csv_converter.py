import csv
import platform
import argparse

parser = argparse.ArgumentParser(description="Converts data exported from COMSOL from .txt to .csv format.")
parser.add_argument('-i', dest='input', action='store', help='input file location')
parser.add_argument('-o', dest='output', action='store', help='output file location')



def read_txt_file(inputfile):
  with open(inputfile) as f:
    #read each line and strip newline character
    lines = [x.strip() for x in f.readlines()]
    #Removes header data
    lines = [line for line in lines if line[0] != '%']
    #split on " " and discard empty strings
    lines = [list(filter(None,line.split(" "))) for line in lines]
    # convert each value on each line into a float
    # Handles E notation
    lines = [[float(string) for string in line] for line in lines]
    return(lines)

def write_csv_file(outputfile, lines):
  with open(outputfile, 'w') as csvfile:

    #Handling case of running script on windows inputting incorrect newline character.
    if 'Windows' in platform.system():
      fieldwriter = csv.writer(csvfile, delimiter=',',quotechar='|', lineterminator="\n", quoting=csv.QUOTE_MINIMAL)
    else:
      fieldwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)

    #writes lines in csv format
    for line in lines:
      fieldwriter.writerow(line)

def convert_file(inputfile, outputfile):
  lines = read_txt_file(inputfile)
  write_csv_file(outputfile, lines)

if __name__ == "__main__":
  args = parser.parse_args()
  convert_file(args.input, args.output)
