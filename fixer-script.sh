#!/bin/bash

SCRIPTDIR="$( cd "$(dirname "$0")" ; pwd -P )"
SCRIPTNAME="/csv-fixer.py"
SCRIPTPATH=$SCRIPTDIR$SCRIPTNAME

DIRECTORY="$(pwd)"

echo $DIRECTORY
echo "$SCRIPTPATH"
echo

for file in "$DIRECTORY"/*.csv; do
  echo $file
  [ -e "$file" ] || continue
  [ -e "${DIRECTORY}/fixed" ] || mkdir "${DIRECTORY}/fixed"
  filename=${file##*/} 
  echo 'Fixing' $filename
  python3 "${SCRIPTPATH}" -i "$filename" -o "${DIRECTORY}/$filename"
  echo
done
