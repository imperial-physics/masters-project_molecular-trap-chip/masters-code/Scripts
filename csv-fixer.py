#!/usr/bin/env python

import csv
import argparse

parser = argparse.ArgumentParser(description="Converts data exported from COMSOL from .txt to .csv format.")
parser.add_argument('-i', dest='input', action='store', help='input file location')
parser.add_argument('-o', dest='output', action='store', help='output file location')

args = parser.parse_args()

def convert_file(inputfile, outputfile):
  with open(inputfile) as f:
    #read each line and strip newline character
    lines = [x.strip() for x in f.readlines()]
    #Removes header data
    lines = [line for line in lines if line != ""]

  with open(outputfile, 'w', newline='') as csvfile:
    for line in lines:
      csvfile.write(line+'\n')

if __name__ == "__main__":
  convert_file(args.input, args.output)
